<?php
    session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="chinhsua.css">
    <title>DSSV</title>
    
</head>

<?php
    $is_page_refreshed = (isset($_SERVER['HTTP_CACHE_CONTROL']) && $_SERVER['HTTP_CACHE_CONTROL'] == 'max-age=0');
    
    if (isset($_POST['select_khoa'])) {
        $_SESSION['select_khoa'] = $_POST['select_khoa'];
    }
    if (isset($_POST['key'])){
        $_SESSION['key'] = $_POST['key'];
    }
?>


<body>
    
    <form method="post" enctype="multipart/form-data" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">

        <div class="header">
            <div class="header1">

                <div class="item">

                    <div class="item_left">Khoa</div>

                    <div class="item_right" >
                        <select class="drop" name ='select_khoa' id="select_khoa">
                            <?php
                                $phankhoa = array(
                                    "MTA" => "Khoa học máy tính",
                                    "KDL" => "Khoa học dữ liệu"

                                );
                               
                                echo "<option value=''</option>";
                                foreach($phankhoa as $class => $value) {
                                    echo "<option value= '". $class . "'";
                                    if(array_key_exists('search',$_POST)) {
                                        if ($is_page_refreshed){
                                            if ($_SESSION['select_khoa'] === $class) {
                                                echo "selected";   
                                            }
                                        }
                                    }    
                                         
                                    echo ">$value</option>";
                                }
                                  
                            ?>   
                        </select>
                    </div>
                    
                </div>

                <div class="item">

                    <div class="item_left" >Từ khóa</div>

                    <div class="item_right">
                        <input type="text" class="drop key" name ="key" id ="text" 
                        <?php
                            if(array_key_exists("search",$_POST)) {
                                if ($is_page_refreshed){
                                    echo "value='" . $_SESSION["key"] ."'";
                                }
                            }
                        ?>
                        >
                        
                    </div>

                </div>
                <div class="item item_seach">
                    <button id="button" class="search delete">Xóa</button>
                    <button name="search"id ="search" class="search">Tìm kiếm</button>

                </div>



                <!-- clear các giá trị khoa và khóa -->
                

            </div>

            <div class="item left show_sv">Số sinh viên tìm thấy: xxx</div>
            
            <button class="add"><a href="submit.php" class="them">Thêm</a></button>
            <div class="header2">
                <table class="item_table">
                    <thead>
                        <tr>
                            <th class="table_left">No</th>
                            <th class="table_left">Tên sinh viên</th>
                            <th class="table_left">Khoa</th>
                            <th colspan="2" class="table_right1">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="table_left">1</td>
                            <td class="table_left">Nguyễn văn A</td>
                            <td class="table_left">Khoa học máy tính</td>
                            <td class="table_right"><button class="table_right">xóa</button></td>
                            <td class="table_right"><button class="table_right">sửa</button></td>
                        </tr>
                        <tr>
                            <td class="table_left">2</td>
                            <td class="table_left">Nguyễn văn B</td>
                            <td class="table_left">Khoa học tin</td>
                            <td class="table_right"><button class="table_right">xóa</button></td>
                            <td class="table_right"><button class="table_right">sửa</button></td>
                        </tr>
                        <tr>
                            <td class="table_left">3</td>
                            <td class="table_left">Lê Văn C</td>
                            <td class="table_left">Khoa học dữ liệu</td>
                            <td class="table_right"><button class="table_right">xóa</button></td>
                            <td class="table_right"><button class="table_right">sửa</button></td>
                        </tr>
                        <tr>
                            <td class="table_left">4</td>
                            <td class="table_left">Lê Văn D</td>
                            <td class="table_left">Khoa học dữ liệu</td>
                            <td class="table_right"><button class="table_right">xóa</button></td>
                            <td class="table_right"><button class="table_right">sửa</button></td>
                        </tr>

                    </tbody>
                    
                </table>
            
            </div> 
        </div>
        
    
    </form>
    
    </body>
</html>